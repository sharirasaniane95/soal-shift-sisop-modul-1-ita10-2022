#!/bin/bash
#2a
mkdir -p forensic_log_website_daffainfo_log

#2b
awk -F: 'END {printf "rata-rata serangan adalah sebanyak %f\n",(NR-1)/12}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt



#2c 
awk 'BEGIN{FS = ":"}
    {if (count[$1]++ >= max) max = count[$1]}
    END{ss
        for(i in count)
            if (max == count[i]) 
                print "IP yang paling banyak mengakses server adalah:", 
                    substr(i, 2, length(i) - 2), "sebanyak", count[i], "request."
    }
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt

#2d
awk 'BEGIN{FS=":"}
    match($NF, /curl/) {count++} 
    END{print "\nAda", count, "requests yang menggunakan curl sebagai user agent"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

#2e
awk 'BEGIN{FS=":"}
    {if($3 == "02" && substr($2,2,2) == "22")
    print "\n"substr($1, 2, length($1)-2)}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt