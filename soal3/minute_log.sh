#!/bin/env bash

OUTPUT_PATH="/home/jovan/log/metrics_$(date +"%Y%m%d%H%M%S").log" 

echo \
"mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" \
> $OUTPUT_PATH

MEM="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
SWAP="$(free -m | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')"
STORAGE="$(du -sh /home/jovan/ | awk '{printf "%s,%s",$2,$1}')"

echo "$MEM,$SWAP,$STORAGE" >> $OUTPUT_PATH

chmod 700 $OUTPUT_PATH

# cron job: * * * * * /home/jovan/SisOp/soal-shift-sisop-modul-1-ita01-2022/soal3/minute_log.sh
