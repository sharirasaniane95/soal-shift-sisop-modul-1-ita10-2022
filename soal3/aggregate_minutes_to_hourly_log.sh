#!/bin/env bash

OUTPUT_PATH="/home/jovan/log/metrics_agg_$(date +"%Y%m%d%H").log" 

location="/home/jovan"

function listFile(){
for file in $(ls /home/jovan/log/metrics_* | grep -v agg | grep $(date +"%Y%m%d%H")); do cat $file | grep -v mem; done
}

mem_total_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$1
        }; 
    if($1>max) {
        max=$1
    };
    if($1<min) {
        min=$1
    };
    total+=$1; count+=1
} END {
    print min,max,total/count
}')

mem_total_min=$(echo $mem_total_sorting | awk '{print $1}' | tr ',' '.')
mem_total_max=$(echo $mem_total_sorting | awk '{print $2}' | tr ',' '.')
mem_total_avg=$(echo $mem_total_sorting | awk '{print $3}' | tr ',' '.')

mem_used_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$2
        }; 
    if($2>max) {
        max=$2
    };
    if($2<min) {
        min=$2
    };
    total+=$2; count+=1
} END {
    print min,max,total/count
}')

mem_used_min=$(echo $mem_used_sorting | awk '{print $1}' | tr ',' '.')
mem_used_max=$(echo $mem_used_sorting | awk '{print $2}' | tr ',' '.')
mem_used_avg=$(echo $mem_used_sorting | awk '{print $3}' | tr ',' '.')

mem_free_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$3
        }; 
    if($3>max) {
        max=$3
    };
    if($3<min) {
        min=$3
    };

    total+=$3; count+=1
} END {
    print min,max,total/count
}')

mem_free_min=$(echo $mem_free_sorting | awk '{print $1}' | tr ',' '.')
mem_free_max=$(echo $mem_free_sorting | awk '{print $2}' | tr ',' '.')
mem_free_avg=$(echo $mem_free_sorting | awk '{print $3}' | tr ',' '.')

mem_shared_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$4
        }; 
    if($4>max) {
        max=$4
    };
    if($4<min) {
        min=$4
    };
    total+=$4; count+=1
} END {
    print min,max,total/count
}')

mem_shared_min=$(echo $mem_shared_sorting | awk '{print $1}' | tr ',' '.')
mem_shared_max=$(echo $mem_shared_sorting | awk '{print $2}' | tr ',' '.')
mem_shared_avg=$(echo $mem_shared_sorting | awk '{print $3}' | tr ',' '.')

mem_buff_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$5
        }; 
    if($5>max) {
        max=$5
    };
    if($5<min) {
        min=$5
    };
    total+=$5; count+=1
} END {
    print min,max,total/count
}')

mem_buff_min=$(echo $mem_buff_sorting | awk '{print $1}' | tr ',' '.')
mem_buff_max=$(echo $mem_buff_sorting | awk '{print $2}' | tr ',' '.')
mem_buff_avg=$(echo $mem_buff_sorting | awk '{print $3}' | tr ',' '.')

mem_available_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$6
        }; 
    if($6>max) {
        max=$6
    };
    if($6<min) {
        min=$6
    };
    total+=$6; count+=1
} END {
    print min,max,total/count
}')

mem_available_min=$(echo $mem_available_sorting | awk '{print $1}' | tr ',' '.')
mem_available_max=$(echo $mem_available_sorting | awk '{print $2}' | tr ',' '.')
mem_available_avg=$(echo $mem_available_sorting | awk '{print $3}' | tr ',' '.')

swap_total_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$7
        }; 
    if($7>max) {
        max=$7
    };
    if($7<min) {
        min=$7
    };
    total+=$7; count+=1
} END {
    print min,max,total/count
}')

swap_total_min=$(echo $swap_total_sorting | awk '{print $1}' | tr ',' '.')
swap_total_max=$(echo $swap_total_sorting | awk '{print $2}' | tr ',' '.')
swap_total_avg=$(echo $swap_total_sorting | awk '{print $3}' | tr ',' '.')

swap_used_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$8
        }; 
    if($8>max) {
        max=$8
    };
    if($8<min) {
        min=$8
    };
    total+=$8; count+=1
} END {
    print min,max,total/count
}')

swap_used_min=$(echo $swap_used_sorting | awk '{print $1}' | tr ',' '.')
swap_used_max=$(echo $swap_used_sorting | awk '{print $2}' | tr ',' '.')
swap_used_avg=$(echo $swap_used_sorting | awk '{print $3}' | tr ',' '.')

swap_free_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$9
        }; 
    if($9>max) {
        max=$9
    };
    if($9<min) {
        min=$9
    };
    total+=$9; count+=1
} END {
    print min,max,total/count
}')

swap_free_min=$(echo $swap_free_sorting | awk '{print $1}' | tr ',' '.')
swap_free_max=$(echo $swap_free_sorting | awk '{print $2}' | tr ',' '.')
swap_free_avg=$(echo $swap_free_sorting | awk '{print $3}' | tr ',' '.')

path_size_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$11
        }; 
    if($11>max) {
        max=$11
    };
    if($11<min) {
        min=$11
    };
    total+=$11; count+=1
} END {
    print min,max,total/count
}')

path_size_min=$(echo $path_size_sorting | awk '{print $1}' | tr ',' '.')
path_size_max=$(echo $path_size_sorting | awk '{print $2}' | tr ',' '.')
path_size_avg=$(echo $path_size_sorting | awk '{print $3}' | tr ',' '.')

echo \
"type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" \
> $OUTPUT_PATH

echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$location,$path_size_min" >> $OUTPUT_PATH
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$location,$path_size_max" >> $OUTPUT_PATH
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$location,$path_size_avg" >> $OUTPUT_PATH

chmod 700 $OUTPUT_PATH

#cron job: 0 * * * * /home/jovan/SisOp/soal-shift-sisop-modul-1-ita01-2022/soal3/aggregate_minutes_to_hourly_log.sh
