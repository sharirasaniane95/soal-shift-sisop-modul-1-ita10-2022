# :zap: **soal-shift-sisop-modul-1-ITA10-2022** :zap:

| Nama                      | NRP        |
|---------------------------|------------|
| Muhammad Firdho Kustiawan | 5027201005 |
| Sharira Saniane           | 5027201016 |
| Jovan Surya Bako          | 5027201013 | 
<br/>


## :large_blue_circle: **Soal 1** :large_blue_circle: 

## **1a**
Diminta untuk membuat file register.sh yang berfungsi mendaftarkan user kemudian membuat file user.txt di folder users untuk menyimpan username dan password, dan membuat file main.sh untuk login user yang sudah terdaftar.

- Sistem register

    ```
    if [ ! -d log.txt ]; then
        touch log.txt
    fi

    if [ ! -d ./users ]; then
        mkdir ./users
    fi

    if [ ! -d ./users/user.txt ]; then
        touch ./users/user.txt
    fi

    time="$(date +'%D %H:%M:%S')"
    ```
- ``-d`` berfungsi untuk pengecekan file/directory 
- variabel time berisikan tanggal,jam,menit, dan detik untuk mempermudah mengisikan laporan pada log.txt

Melakukan pengecekan apakah folder log.txt dan folder users yang di dalamnya terdapat file user.txt sudah terbuat atau belum, jika belum maka akan langsung dibuat




### 1b
pada tahap register, password yang diisikan harus memiliki kriteria yang diminta, yaitu:
i. minimal 8 karakter : 
```shell
[ `expr length "$password"` -lt 8 ] 
```
pada bagian expr length password akan menghitung jumlah huruf pada password dan -lt 8 : Less Then berfungsi untuk mengecek apakah passwordg telah dihitung tadi kurang dari 8.

ii. memiliki minimal 1 huruf kapital dan 1 huruf kecil: 

```shell
if [[ $password = ${password^^} ]] ; then 
   echo "Password minimal 1 huruf kecil" 
```

 pada bagian ini passwordnya akan disamakan dengan password yang telah dijadikan huruf kapital semuanya. jikalau sama maka akan menampilkan pesan "Password minimal 1 huruf kecil".
    

```shell
elif [[ $password = ${password,,} ]] ; then 
   echo "Password minimal 1 huruf kecil" 
fi
```
 pada bagian ini passwordnya akan disamakan dengan password yang telah dijadikan huruf kecil semuanya. jikalau sama maka akan menampilkan pesan "Password minimal 1 huruf kapital".

iii. Alphanumeric
```shell
if ! [[ "$password" =~ [0-9] ]] ; then 
   echo "Password harus ada angkanya"
elif
```
pada bagian ini password akan di cek menggunakan "=~", apakah password tersebut tidak terdapat angka 0-9 . jikalau tidak ada maka akan menampilkan pesan "password harus ada angkanya"

iv. tidak boleh sama dengan username.
```shell
elif
[ $username == $password ]; then 
     echo "Password tidak boleh sama dengan username"
```
pada bagian ini akan di cek apakah password dan usernamenya sama atau tidak.
jika iya akan ada pesan "Password tidak boleh sama dengan username".

```shell
   echo "$username $password" >> ./users/user.txt && echo "$username berhasil ditambahkan" &&  echo "$time REGISTER: INFO User $username registered successfully" >> log.txt
```
setelah berhasil terdaftar akan ada pesan  $username berhasil ditambahkan", username dan passwordnya akan disimpan di file user.txt, dan akan ada laporan bahwa ada user yang telah terdaftar pada log.txt
![Output result](img/1.png)



## 1c
setiap percobaan login dan register akan disimpan pada file log.txt

i. jika username yang dimasukan sudah ada yang terdaftar maka akan ditampilkan pesan kalau user telah terdaftar lalu laporannya akan dimasukan ke log.txt

 ```shell
 if grep -q "$username" ./users/user.txt ; then
   echo "User $username sudah ada"
   echo "$time REGISTER: ERROR $username already exists" >> log.txt 
```
- ``grep -q`` berfungsi untuk mencari kata/kalimat yang ditunjuk pada sebuah file

![Output result](img/2.png)

ii. ketika precobaan register berhasil maka akan ditambahkan laporan "username register successfully" pada log.txt
![Output result](img/13.png)

iii. ketika user mencoba login dan password yang diisikan salah maka akan dilaporkan bahwa user tersebut gagal login
```shell
 else
     echo "Login tidak berhasil"
     echo "$time LOGIN: ERROR Failed login attempt on user $username"  >> log.txt
     exit 1
 fi
```
![Output result](img/11.png)

iv. ketika user berhasil login maka akan ada laporan ke log.txt kalau user tersebut telah berhasil login
![Output result](img/3.png)


## 1d. 
setelah berhasil login akan muncul 2 opsi, yaitu 1.mendownload gambar dan 2.hitung jumlah login yang gagal dan berhasil

```shell
  folder=$(date +'%Y-%m-%d')_$username
```
- variabel folder berisikan tahun,bulan,tanggal hari_username yang sedang login. Variabel ini dibuat agar mempermudah penamaan folder dan file zip yang nantinya dibuat

i. pada opsi pertama diminta untuk masukan berapa jumlah gambar yang ingin di download dari link yang telah diberikan
gambar yang telah di download akan dimasukan ke sebuah foder yang bernama "tahun-bulan-tanggal hari_username", gambar yang telah di download pun namanya diganti menjadi PIC_xx dengan nomor yang berurutan sesuai dengan jumlah yang didownload.
setelah itu folder yang menyimpan gambar-gambar tersebut di zipkan dengan nama sesuai dengan foldernya dan juga diberi password dari user yang sedang login.
```shell
    elif ! [ -e ${folder}.zip ] ;then
    mkdir $folder

        for ((num=1; num<=sum; num=num+1))
        do  
            if [ $num -lt 10 ] ; then
                echo "PIC_0$num"
                wget https://loremflickr.com/320/240 -O PIC_0$num.jpeg
                mv -n PIC_0$num.jpeg ./$folder
            else
                echo "PIC_$num"
                wget https://loremflickr.com/320/240 -O PIC_$num.jpeg 
                mv -n PIC_0$num.jpeg ./$folder
            fi
        done
        
       zip -rP $password $folder.zip $folder 
        rm -r $folder
        fi 
```
- ``-o`` berfungsi untuk rename file
- ``-n`` berfungsi untuk menjalankan perintah tanpa meminta ijin
- ``-e`` berfungsi untuk mencari file/directory yang ditunjuk
- ``zip`` berfungsi untuk zip folder/file yang ditunjuk

- ``-r`` berfungsi untuk menzip file secara terstruktur
- ``-P`` untuk zip berfungsi untuk menambahkan password sesuai dengan yang diberikan
- ``mv`` memindahkan file


awalnya akan dicek terlebih dahulu apakah tidak ada file zip yang bernama sama. jika tidak ada maka akan langsung dilanjutkan dengan pembuatan directory untuk menyimpan gambar. setelah itu akan masuk ke perulangan for yang akan mendownload gambarnya sesuai dengan jumlah yang diberikan. lalu masuk ke if else yang mana akan di cek terlebih dahulu apakah jumlah gambar yang di download lebih dari 10 atau tidak. jika kurang dari 10 maka nama gambarnya akan menjadi "PIC_0$num" jika lebih dari 10 maka 0nya yang ada didepan akan dihapus menjadi "PIC_$num".
setelah namanya diganti maka akan langsung dipindahken ke folder yang sudah tersedia, setelah itu folder tersebut akan di zip lalu akan dihapus foldernya
![Output result](img/5.png)

selanjutnya jika ada file zip yang namanya sama.
```shell
if [ -e ${folder}.zip ] ;then
     unzip -P $password $folder.zip 

             for ((num=1; num<=sum; num=num+1))
        do  
            if [ $num -lt 10 ] ; then
                echo "PIC_0$num"
                wget https://loremflickr.com/320/240 -O PIC_S0$num.jpeg
                mv -n PIC_S0$num.jpeg ./$folder
            else
                echo "PIC_$num"
                wget https://loremflickr.com/320/240 -O PIC_S$num.jpeg 
                mv -n PIC_S$num.jpeg ./$folder
            fi
        done
        zip -rP $password $folder.zip $folder 
        rm -r $folder
```

sama seperti sebelumnya, awalnya akan dicek terlebih dahulu apakah ada zip yang bernama sama, jika ada maka akan di unzip il zip tersebut lalu akan masuk ke perulangan for untuk mendownload gambarnya.
bedanya nama gambar tersebut ditambahkan huruf "S" di depan urutan gambarnya, hal tersebut untuk mencegah apa bila ada file yang sama dan diminta untuk direwrite/replace
setelah itu akan di zip foldernya lalu di hapus folder tersebut 
![Output result](img/6.png)

ii. pada opsi ke dua diminta untuk menghitung jumlah login yang berhasil dan yang gagal
```shell
 elif [ $option -eq 2 ] ; then
    printf "Total login yang berhasil: " 
    grep "LOGIN: INFO User $username logged in" log.txt | wc -l
        printf "\nTotal login yang tidak berhasil: "  
        grep "LOGIN: ERROR Failed login attempt on user $username" log.txt | wc -l
```
 - `wc -l` Word Count berfungsi untuk menghitung kata/kalimat yang muncul pada sebuah file, -l untuk menghitung jumlah line. keduanya dikombinasikan 

 fungsi grep digunakan untuk mencari laporan yang berhasil dan gagal login dengan username yang sama dengan yang sedang login pada file log.txt
 ![Output result](img/14.jpg)






## :large_blue_circle: **Soal 2** :large_blue_circle: 
Pada soal no 2 terdapat file *log_website_daffainfo.log* yang diberikan untuk diolah dengan menggunakan script awk, dengan ketentuan:

## 2a
Soal -> Memuat folder bernama `forensic_log_website_daffainfo_log` 
Penyelesaian -> syntax yang digunakan yaitu : 
```sh
mkdir -p forensic_log_website_daffainfo_log
```
- `mkdir` berfungsi untuk membuat sebuah folder/directory 
- `-p`  berfungsi untuk berfungsi untuk agar tidak terjadi error jika folder tersebut sudah ada
- `forensic_log_website_daffainfo_log` merupakan nama folder yang diminta pada soal

## 2b
Soal -> Mencari **rata-rata request per jam** kemudian memasukkan output-nya ke dalam file bernama `ratarata.txt` ke dalam folder 2a
Penyelesaian -> Untuk mendapatkan nilai rata - rata per jam, syntax yang kami gunakan sebagai berik
``sh
awk -F: 'END {printf "rata-rata serangan adalah sebanyak %f\n",(NR-1)/12}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt
``
- `awk` berfungsi untuk melakukan pencetakan dari data program diatas
- `-F` merupakan  option separator berfungsi Nentuin pembatas
- `NR` merupakan jumlah baris pada file `log_website_daffainfo.log`, alasan mengapa dikurangi -1 yaitu dikarenakan baris pertama pada file log berisi nama setiap kolom
- `/12` merupakan perbedaan jam antara data pertama dengan data terakhir  yang berfungsi untuk mencari nilai rata - ratanya 
- Hasil pembagian tersebut akan dimasukkan ke dalam file `ratarata.txt` yang berada pada folder **forensic_log_website_daffainfo_log**

## Output 
Berikut merupakan output dari soal 2b : 

## 2c 
Soal -> Menampilkan **IP yang paling banyak** melakukan request ke server beserta **jumlah requestnya**. Lalu, memasukkan output-nya ke dalam file baru bernama `result.txt` ke dalam folder 2a.
Penyelesaian -> untuk penyelesaiannya kami menggunakan script sebagai berikut : 
```sh
awk 'BEGIN{FS = ":"}
    {if (count[$1]++ >= max) max = count[$1]}
    END{
        for(i in count)
            if (max == count[i]) 
                print "IP yang paling banyak mengakses server adalah:", 
                    substr(i, 2, length(i) - 2), "sebanyak", count[i], "request."
    }
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt
```
- awk dimulai dengan `BEGIN {FS = ":"}`merupakan dimana saat ingin hendak mengloop pada file dimulai kita melakukan `FS` yang berfungsi digunakana untuk membuat pemisahnya. contoh pada script diatas yaitu `:` 
- `if (count[$1]++ >= max) max = count[$1]}`, terdapat variabel array bernama `count` dengan array key IP dan variabel `max` berfungsi menampung jumlah IP terbesar yang sering muncul. awk akan melakukan perulangan pada data selanjutnya menemukan IP yang sama maka nilai akan bertambah, jika `count` dari IP  berbeda  kemudian memiliki nilai lebih besar maupun sama maka nilai `max` akan berubah menjadi nilai `count` tersebut. 
-  `for(i in count)` dimana loop untuk setiap array `i`
- `if (max == count[i])` dimana ketika awk menemukan nilai max, maka menjaankan script `print "IP yang paling banyak mengakses server adalah:" substr(i, 2, length(i) - 2), "sebanyak", count[i], "request."`
- `substr` berfungsi untuk memotong string, pada script ini berfungsi untuk menghilangkan tanda petik 2 yang terletak pada kolom pertama. `lenght` berfungsi mencari jumlah hurufnya 
- Seluruh hasil script awk tersebut akan dimasukkan ke dalam file result.txt (baris pertama) di dalam folder forensic_log_website_daffainfo_log

## 2d 
Soal -> Menghitung banyaknya request, berapa banyak requests yang menggunakan user-agent curl. Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt 
Penyelesaian -> menggunakan script sebagai berikut :
```sh
awk 'BEGIN{FS=":"}
    /curl/ {count++} 
    END{print "\nAda", count, "requests yang menggunakan curl sebagai user agent"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
- Mendeklasrasikan `FS=":"` 
- kemudian `/curl/ {count++}` untuk mnecari user agent yang bernama curl 
- Terakhir pada loop berakhir akan mencetak tulisan sesuai dengan format dan dimasukkan ke dalam file `result.txt` (baris terakhir) yang berada pada folder *forensic_log_website_daffainfo_log*

## 2e
Soal -> Daftar IP yang mengakses website pada jam 2 pagi tanggal 22. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya
Penyelesaian -> untuk penyelesaiannya kami menggunakan script : 
```sh
awk 'BEGIN{FS=":"}
    {if($3 == "02" && substr($2,2,2) == "22")
    print "\n"substr($1, 2, length($1)-2)}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
-  proses membaca file log_website_daffainfo.log adalah menggunakan awk 
- `BEGIN{FS=":"}` berfungsi memberi tahu `FS` pada dari file log_website_daffainfo.log adalah `:`
- `if($3 == "02" && substr($2,2,2) == "22")` dimana maksud dari script ini $3 kolom ketiga dengan **jam 02**, kolom kedua menghasilkan **tanggal 22**
- Selanjutnya akan melakukan `print "\n"substr($1, 2, length($1)-2)` untuk menghapus tanda petik 2 dari IP yang ada
- Terakhir hasil dari awk nya akan ke dalam file `result.txt` (baris terakhir) yang berada pada folder *forensic_log_daffainfo_log*

## Output 
Berikut adalah screenshot output dari semua jawaban : 
![Output result](img/2.5.jpg)
![Output result](img/2b.png)
![Output result](img/2c.png)
![Output result](img/2d.png)


## :large_blue_circle: **Soal 3** :large_blue_circle:

## 3 a
soal ->  Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
Penyelesaian -> pada soal ini kami menggunakan script : 

sh
#!/bin/env bash

OUTPUT_PATH="/home/dho/log/metrics_$(date +"%Y%m%d%H%M%S").log" 

echo \
"mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" \
> $OUTPUT_PATH

MEM="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
SWAP="$(free -m | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')"
STORAGE="$(du -sh /home/dho/ | awk '{printf "%s,%s",$2,$1}')"

echo "$MEM,$SWAP,$STORAGE" >> $OUTPUT_PATH

chmod 700 $OUTPUT_PATH
Sebelum melanjutkan pada penjelasan script kelompok kami akan menjelaskan terlebih dahulu apa itu  command free -m dan du-sh
-  command free -m berfugsi untuk menampilkan monitoring memory pada suatu directory 
-  du-sh berfungsi untuk menampilkan suatu path dan path size nya 
kemudian kelompok kami menggunakan konsep awk untuk memperoleh data data yang sesuai dengan format yang ditentukan. Adapun konsep awk ini melakukan pengecekkan dan pengambilan data perbaris. 
selanjutnya kami membuat sebuah variabel untuk menyimpan nilai dari path **/home/user/log**, dimana dalam directory log terdapat suatu file yang bernama **metrics_{YmdHms}.log** 

sh
OUTPUT_PATH="/home/dho/log/metrics_$(date +"%Y%m%d%H%M%S").log" 
lalu, kami akan menampilkan string dengan menggunakan perintah echo. Adapun string yang akan ditampilkan yaitu : 
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size.
kemudian string tersebut akan disimpan pada variabel `OUTPUT_PATH`. 

sh 
echo \
"mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" \
> $OUTPUT_PATH
kemudian, kami akan mencoba untuk memperoleh data memory dengan menggunakan command free -m yang akan digabungkan dengan konsep awk, dimana hanya akan menampilkan data dari memory dengan format `/Mem:/`. Output yang akan ditampilkan akan dimulai dari kolom kedua higga kolom ketujuh. Setelah itu, data yang didapatkan tadi disimpan pada variabel MEM. 

sh
MEM="$(free -m | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
Selanjutnya, kami akan mencoba untuk memperoleh data swap dengan menggunakan command free -m yang akan digabungkan dengan konsep awk, dimana hanya akan menampilkan data dari memory dengan format `/Swap:/`. Output yang akan ditampilkan akan dimulai dari kolom kedua higga kolom keempat. Setelah itu, data yang didapatkan tadi disimpan pada variabel SWAP. 

sh 
SWAP="$(free -m | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')"
Lalu, untuk memperoleh data path dan data path size, kami akan menggunakan command du -sh<target path> adapun target path nya adalah /home/user/. Setelah itu, kami menggunakan print terhadap input argunmentasinya. input argumentasi yang diminta yaitu path dan diikuti oleh path size, sedangkan jika menggunakan command tadi yaitu path size terlebih dahulu dan diikuti dengan nama path. Oleh sebab itu, kami akan melakukan print dari kolom kedua lalu kolom kesatu dan data tersebut disimpan pada variabel yang bernama STORAGE.

sh 
STORAGE="$(du -sh /home/dho/ | awk '{printf "%s,%s",$2,$1}')"
Kemudian, setelah mendapatkan ketiga data tersebut kami akan melakukan print dengan menggunakan command echo dan menyimpannya pada variabel OUTPUT_PATH menggunakan `>>` dimana berfungsi untuk menambahkan suatu karakter/nilai meskipun data tersebut telah diisi oleh karakter/nilai terlebih dahulu.

sh 
echo "$MEM,$SWAP,$STORAGE" >> $OUTPUT_PATH

Untuk pengubahan terhadap akses file dimana hanya user yang dapat mengakses file (read, write, execute) dari `$OUTPUT_PATH)`. 

sh
chmod 700 $OUTPUT_PATH


## 3b 
soal -> Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
Penyelesaian -> Untuk menjalakan peritah yag diminta oleh soal, kami akan menggunakan cron dengan menulis crontab -e berfungsi membuat crontab baru. Sript pada cron akan menuliskan script yag ingin kita jalankan setiap menit menggunakan `* * * * * /home/jovan/praktikum/soal-shift-sisop-modul-1-ita01-2022/soal3/minute_log.sh`. script tersebut akan menghasilkan filr .log menggunakkan format metrics_{YmdHms}.log berisi catatan dari metrics dari RAM dan size suatu directory.

## 3c 
Soal -> Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
Penyelesaian :
sh
OUTPUT_PATH="/home/rasy/log/metrics_agg_$(date +"%Y%m%d%H").log" 
Langkah pertama, mmebuat script shell dengan nama `aggregate_minutes_to_hourly_log.sh`. kemudian, file tersebut akan menghasilkan file log `metrics_agg_{YmdH}.log`, dilanjutkan dengan membuat varibel untuk menyimpan output degan nama`OUTPUT_PATH`.

sh
location="/home/rasy"

selanjutnya, mendeklarasikan variabel location yang mempunyai nilai /home/user/ berfungsi untuk menampilkan nama path yang akan dimonitor oleh script yang telah dibuat.

sh
function listFile(){
for file in $(ls /home/rasy/log/metrics_* | grep -v agg | grep $(date +"%Y%m%d%H")); do cat $file | grep -v mem; done
}
Berfungsi untuk mendapatkan data-data tiap menit, dengan membuat sebuah fungsi dengan nama function listFile() dimana akan mencari semua file dengan awalan metrics. Kemudian terdapat pipe yang didalamnya terdapat command grep -v agg yang berfungsi untuk mengeluarkan atau tidak menampilkan file dengan nama agg. setelah itu, terdapat pipe lagi untuk melakukan seleksi pada file di jam yang sama. Di dalam pengulangan tersebut kami mencoba membaca tiap satuan file tetapi hanya print baris yang tidak terdapat kata mem sesuai dengan do cat $file | grep -v mem.

sh
mem_total_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$1
        }; 
    if($1>max) {
        max=$1
    };
    if($1<min) {
        min=$1
    };
    total+=$1; count+=1
} END {
    print min,max,total/count
}')
Script tersebut berfungsi untuk memproses tiap data yang diprint pada `listFile`, selajutnya membuat fungsi mem_total_sorting yang berfugsi untuk menampilkan data minimum, maksimum dan average setiap data per kategorinya di mem_total. Lalu, meggunakan konsep AWK dengan field separator `,`. Kemudian membuat pegkondisian dengan $1 = mem_total maka akan mendapatkan nilai nilai minimum, maksimum dan average. Average akan didapatkan dari jumlah nilai variabel total dibagi dengan jumlah data yang masuk pada mem_total.


Lalu untuk mendapatkan data-data yang sesuai pada mem_total, kami hanya tinggal memanggil fungsi mem_total_sorting dan pass argumen yang sesuai. Lalu disini kami memakai konsep AWK untuk melakukan pass argumen tersebut dan juga menggunakan command tr untuk mengganti nilai yang terdapat komanya (contohnya 2,2) menjadi titik (contohnya 2.2). Pada data-data selanjutnya sepeerti mem_used, mem_free, dan lainnya akan sama seperti mem_total dengan melakukan perubahan pada setiap kolomnya. Untuk kodenya yaitu sebagai berikut:

sh
mem_total_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$1
        }; 
    if($1>max) {
        max=$1
    };
    if($1<min) {
        min=$1
    };
    total+=$1; count+=1
} END {
    print min,max,total/count
}')

mem_total_min=$(echo $mem_total_sorting | awk '{print $1}' | tr ',' '.')
mem_total_max=$(echo $mem_total_sorting | awk '{print $2}' | tr ',' '.')
mem_total_avg=$(echo $mem_total_sorting | awk '{print $3}' | tr ',' '.')

mem_used_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$2
        }; 
    if($2>max) {
        max=$2
    };
    if($2<min) {
        min=$2
    };
    total+=$2; count+=1
} END {
    print min,max,total/count
}')

mem_used_min=$(echo $mem_used_sorting | awk '{print $1}' | tr ',' '.')
mem_used_max=$(echo $mem_used_sorting | awk '{print $2}' | tr ',' '.')
mem_used_avg=$(echo $mem_used_sorting | awk '{print $3}' | tr ',' '.')

mem_free_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$3
        }; 
    if($3>max) {
        max=$3
    };
    if($3<min) {
        min=$3
    };

    total+=$3; count+=1
} END {
    print min,max,total/count
}')

mem_free_min=$(echo $mem_free_sorting | awk '{print $1}' | tr ',' '.')
mem_free_max=$(echo $mem_free_sorting | awk '{print $2}' | tr ',' '.')
mem_free_avg=$(echo $mem_free_sorting | awk '{print $3}' | tr ',' '.')

mem_shared_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$4
        }; 
    if($4>max) {
        max=$4
    };
    if($4<min) {
        min=$4
    };
    total+=$4; count+=1
} END {
    print min,max,total/count
}')

mem_shared_min=$(echo $mem_shared_sorting | awk '{print $1}' | tr ',' '.')
mem_shared_max=$(echo $mem_shared_sorting | awk '{print $2}' | tr ',' '.')
mem_shared_avg=$(echo $mem_shared_sorting | awk '{print $3}' | tr ',' '.')

mem_buff_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$5
        }; 
    if($5>max) {
        max=$5
    };
    if($5<min) {
        min=$5
    };
    total+=$5; count+=1
} END {
    print min,max,total/count
}')

mem_buff_min=$(echo $mem_buff_sorting | awk '{print $1}' | tr ',' '.')
mem_buff_max=$(echo $mem_buff_sorting | awk '{print $2}' | tr ',' '.')
mem_buff_avg=$(echo $mem_buff_sorting | awk '{print $3}' | tr ',' '.')

mem_available_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$6
        }; 
    if($6>max) {
        max=$6
    };
    if($6<min) {
        min=$6
    };
    total+=$6; count+=1
} END {
    print min,max,total/count
}')

mem_available_min=$(echo $mem_available_sorting | awk '{print $1}' | tr ',' '.')
mem_available_max=$(echo $mem_available_sorting | awk '{print $2}' | tr ',' '.')
mem_available_avg=$(echo $mem_available_sorting | awk '{print $3}' | tr ',' '.')

swap_total_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$7
        }; 
    if($7>max) {
        max=$7
    };
    if($7<min) {
        min=$7
    };
    total+=$7; count+=1
} END {
    print min,max,total/count
}')

swap_total_min=$(echo $swap_total_sorting | awk '{print $1}' | tr ',' '.')
swap_total_max=$(echo $swap_total_sorting | awk '{print $2}' | tr ',' '.')
swap_total_avg=$(echo $swap_total_sorting | awk '{print $3}' | tr ',' '.')

swap_used_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$8
        }; 
    if($8>max) {
        max=$8
    };
    if($8<min) {
        min=$8
    };
    total+=$8; count+=1
} END {
    print min,max,total/count
}')

swap_used_min=$(echo $swap_used_sorting | awk '{print $1}' | tr ',' '.')
swap_used_max=$(echo $swap_used_sorting | awk '{print $2}' | tr ',' '.')
swap_used_avg=$(echo $swap_used_sorting | awk '{print $3}' | tr ',' '.')

swap_free_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$9
        }; 
    if($9>max) {
        max=$9
    };
    if($9<min) {
        min=$9
    };
    total+=$9; count+=1
} END {
    print min,max,total/count
}')

swap_free_min=$(echo $swap_free_sorting | awk '{print $1}' | tr ',' '.')
swap_free_max=$(echo $swap_free_sorting | awk '{print $2}' | tr ',' '.')
swap_free_avg=$(echo $swap_free_sorting | awk '{print $3}' | tr ',' '.')

path_size_sorting=$(listFile | awk -F, '
{
    if(min==""){
        min=max=$11
        }; 
    if($11>max) {
        max=$11
    };
    if($11<min) {
        min=$11
    };
    total+=$11; count+=1
} END {
    print min,max,total/count
}')

path_size_min=$(echo $path_size_sorting | awk '{print $1}' | tr ',' '.')
path_size_max=$(echo $path_size_sorting | awk '{print $2}' | tr ',' '.')
path_size_avg=$(echo $path_size_sorting | awk '{print $3}' | tr ',' '.')

Kemudian, kami akan menampilkan string dengan menggunaka perintah `echo`. Untuk string yang ditampilkan yaitu:
sh
echo \
"type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

setelah itu akan disimpan pada variabel `OUTPUT_PATH`. selanjutnya akan menampilkan data-data dari semua nilai variabel diatas serta akan disimpan juga pada variabel `OUTPUT_PATH`.

sh
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$location,$path_size_min" >> $OUTPUT_PATH
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$location,$path_size_max" >> $OUTPUT_PATH
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$location,$path_size_avg" >> $OUTPUT_PATH

Pada akhir script dituliskan chmod 700 $OUTPUT_PATH untuk pengubahan terhadap akses file dimana hanya user yang dapat mengakses file (read, write, execute) dari `$OUTPUT_PATH)`.


## 3d 
Soal -> Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
Penyelesaian : 
Penyelesaian yang dapat dilakukan ialah dengan menjalankan command `chmod 700 $OUTPUT_PATH`, lalu, akan terdapat angka 7 dimana user mendapatkan akses untuk read, write, dan execute. Pada kedua 0 setelah angka 7 menunjukkan group dan other tidak dapat melakukan pengaksesan terhadap file (read, write, dan execute)
 
 ### hasil dari nomer 3
![Output result](img/no3.png)

### Kendala
Kendala yang kami hadapi ialah kesulitan dalam melakukan penalaran logika yang dimaksud pada soal serta kurang menguasai konsep AWK dengan baik.


